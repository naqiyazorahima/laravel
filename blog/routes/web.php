<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/home', function () {
//     return view ('home');
// });

// Route::get('/register', function () {
//     return view ('register');
// });

// Route::get('/welcome', function () {
//     return view ('welcome');
// });

//Route::get('/', 'HomeController@index')->name('home');
//Route::get('/register', 'AuthController@index')->name('register');
//Route::get('/welcome', 'AuthController@dashboard')->name('welcome');
//Route::post('/welcome',  'AuthController@dashboard')->name('welcome');;
//Route::post('/welcome',  'AuthController@dashboard');

Route::get('/', function () {
     return view ('main');
 });

//  Route::get('/data-tables', function () {
//     return view ('data');
// });

//Untuk tugas CRUD
// Route::get('/pertanyaan', 'PertanyaanController@index')->name(''); //done
// Route::get('/pertanyaan/create', 'PertanyaanController@create')->name('create'); //done
// Route::post('/pertanyaan', 'PertanyaanController@store'); //done
// Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show')->name('');  //done
// Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanController@edit')->name(''); //done
// Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update')->name(''); //done
// Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy')->name('');

Route::resource('answers', 'AnswerController');
Route::resource('pertanyaan', 'PertanyaanController');