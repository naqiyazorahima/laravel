@extends('layouts.master')

@section('content')
            <div class="ml-4 mt-3 mr-3">
            <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Question {{$result->id}}</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="/pertanyaan/{{$result->id}}" method="post">
                    @csrf
                    @method('put')
                        <div class="card-body">
                        <div class="form-group">
                            <label for="Judul">Judul</label>
                            <input type="text" class="form-control" id="inputJudul" name= "judul" value= "{{old('judul', $result->judul)}}" placeholder="Enter Title" require>
                            @error('judul')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="Isi">Isi</label>
                            <!-- old masih belum berfungsi -->
                            <input type="text" class="form-control" id="inputIsi" name= "isi"  value= "{{old('isi',$result->isi)}}" placeholder="Enter Isi" require>
                            <!-- <textarea class="form-control"   id="inputIsi" name= "isi" value= "{{old('isi', '')}}" placeholder="Enter Title"  require></textarea> -->
                            
                            @error('isi')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        
                        
                        <!-- /.card-body -->

                        <div class="card-footer">
                        
                        <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
          </div>
            </div>
        
@endsection
