@extends('layouts.master')

@section('content')
        <div class="ml-4 mt-3 mr-3">
        <div class="card">
              <div class="card-header">
                <h3 class="card-title"> Tabel Pertanyaan </h3>
               
              </div>
              <!-- /.card-header -->
              
              <div class="card-body">
                @if(session('success'))
                <div class="alert alert-success">
                {{session('success')}}
                </div>
                @endif
              <div class="mb-2">
              <a class="btn btn-primary" href="{{route('pertanyaan.create')}}" > Create New Question</a>
              </div>
              
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Judul</th>
                      <th>Isi</th>
                      <th style="width: 60px" >Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($post as $key => $post)
                    <tr> 
                        <td> {{$key + 1}}  </td>
                        <td> {{$post ->judul}}  </td>
                        <td> {{$post ->isi}}  </td>
                        <td style= "display : flex"> 
                        <a href="{{route('pertanyaan.show', ['pertanyaan' =>$post->id])}}" class="btn btn-info btn-sm mr-2"> Detail </a>
                        <a href="{{route('pertanyaan.edit', ['pertanyaan' =>$post->id])}}" class="btn btn-warning btn-sm mr-2"> Edit </a>
                        <!-- <a href="/pertanyaan/{{$post->id}}" class="btn btn-danger btn-sm"> Edit </a> -->
                        <form action="{{route('pertanyaan.destroy', ['pertanyaan' =>$post->id])}}" method="post">
                        @csrf
                        @method('delete')
                          <input type="submit" class="btn btn-danger btn-sm" value= "delete">
                        </form>
                        
                        </td>
                    </tr>
                    @empty
                    <tr> 
                        <td collspan="4" align="center"> NO POST  </td>
                        
                    </tr>
                    
                    @endforelse
                   
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div>
            </div>

        </div>
@endsection
