<!DOCTYPE html>
<html>

<head>
    <title> SanberBook </title>
    <meta charset="UTF-8">
</head>

<body>
    <!-- Awal  -->
    <div>
        <h1> Create New Account! </h1>
        <h2> Sign Up Form </h2>

    </div>

    <!-- Form  -->
    <div>
        
        <form action="/welcome" method="post">
        @csrf
            <label for="first_name"> First Name : </label> <br>
            <br> <input type="text" name="first_name" id="first_name">
            <br>

            <br>
            <br>
            <label for="last_name"> Last Name : </label> <br>
            <br> <input type="text"name="last_name" id="last_name">
            <br>

            <br>
            <br>
            <label> Gender</label> <br>
            <input type="radio" name="gender" value="0" checked> Male <br>
            <input type="radio" name="gender" value="1"> Female <br>
            <input type="radio" name="gender" value="1"> Other <br>

            <br>
            <br>
            <label for=""> Nationality </label> <br>
            <br>
            <select>
                <option value="0" checked> Indonesia </option>
                <option value="1"> US </option>
                <option value="2"> Canada </option>
            </select>
            <br>
            <br>

            <label> Langguage Spoken</label> <br>
            <input type="checkbox" name="language" value="0"> Bahasa Indonesia <br>
            <input type="checkbox" name="language" value="1" checked> English <br>
            <input type="checkbox" name="language" value="2"> Other <br>
            <br>
            <br>

            <label for="bio"> Bio</label> <br> <br>
            <textarea rows="8" cols="30">  </textarea>
            <br>
            <br> 

            <button type="submit"  >Sign Up</button>

        </form>
    </div>
</body>

</html>