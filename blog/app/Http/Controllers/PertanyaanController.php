<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    //
    public function index() {
        //$post = DB::table('pertanyaan')->get();
        //dd($post);
        $post = Pertanyaan::all();
        return view('pertanyaan.index', compact('post'));
    }

    public function create() {
        return view('pertanyaan.create');
    }

    public function store(Request $request) {
      // dd($request->all());

      $request->validate([
        "judul" => ['required','unique:pertanyaan'],
        "isi" => ['required'],
      ]);

    //    $query = DB::table('pertanyaan')->insert(
    //        [
    //         "judul" => $request["judul"],
    //         "isi" => $request["isi"], 
    //        ]);
    //$pertanyaan ini objek baru dari Class pertanyaan model
        // $pertanyaan = new Pertanyaan;
        // $pertanyaan->judul = $request["judul"];
        // $pertanyaan->isi = $request["isi"];
        // $pertanyaan->save();
    
            $pertanyaan = Pertanyaan::create([
                "judul" => $request["judul"],
                "isi" => $request["isi"], 
            ]);

           return redirect('/pertanyaan')->with('success', 'Post Succesfully Saved');
        // DB::table('users')->insert(
        //     ['email' => 'john@example.com', 'votes' => 0]
        // );
        //return view('home');
    }

    public function show($pertanyaan_id) {
        // $result = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        //dd($result);
        $result = Pertanyaan::find($pertanyaan_id);
        return view('pertanyaan.detail', compact('result'));
    }

    public function edit($pertanyaan_id) {
        //$result = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        $result = Pertanyaan::find($pertanyaan_id);
        return view('pertanyaan.edit', compact('result'));
    }

    public function update($pertanyaan_id, Request $request) {
        $request->validate([
            "judul" => ['required'],
            "isi" => ['required'],
          ]);

        // $result = DB::table('pertanyaan')
        //             ->where('id', $pertanyaan_id)
        //             ->update([
        //                 "judul" => $request["judul"],
        //                 "isi" => $request["isi"], 
        //             ]);
        $update =Pertanyaan::where('id', $pertanyaan_id)->update([
            "judul" => $request['judul'],
            "isi" => $request['isi'],
        ]);

        return redirect('/pertanyaan')->with('success', 'Question Successfully Updated');
    }

    public function destroy($pertanyaan_id) {
        //$result = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();
        Pertanyaan::destroy($pertanyaan_id);
        return redirect('/pertanyaan')->with('success', 'Question Successfully Deleted');
    }
}
